package com.example.demo.model;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerDTO { // data пеореопределяет equals и hashcode/ AllArgs конструктор создает конструктор
    // со всеми полями/ lombok делает все сам
    private int id;
    private String name;


}
